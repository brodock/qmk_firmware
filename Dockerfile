FROM registry.gitlab.com/brodock/qmk_firmware/qmk-build-image:stable

VOLUME /qmk_firmware
WORKDIR /qmk_firmware
COPY . .

CMD make all:default
