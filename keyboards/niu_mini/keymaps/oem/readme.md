# OEM layout

OEM layout for the NIU mini 40% from [kbdfans](https://kbdfans.com/collections/40).

This keymap intends to integrate with proposed functionality printed on OEM Keycaps
available [here](https://kbdfans.com/collections/oem-profile/products/40-layou-dye-sub-keycaps).
